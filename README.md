# Openiam Cloudformation

This repository contains AWS cloudformation templates to deploy OpenIAM.

To deploy:

1) Run:

```
    export REGION=us-west-2
    aws cloudformation create-stack \
      --stack-name helm \
      --capabilities CAPABILITY_NAMED_IAM \
      --template-url file:///<path_to_this_directory>/openiam.helm.lamda.yaml
      --region ${REGION}
```

2) Run:
```
    export REGION=us-west-2
    aws cloudformation create-stack \
      --stack-name eks \
      --capabilities CAPABILITY_NAMED_IAM \
      --template-url file:///<path_to_this_directory>/aqeks.deploy.template
      --region ${REGION}
```

3) Run:

Import the file `openiam.cloudformation.template` into the [cloudformation UI](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/template), and fill out the required variables.